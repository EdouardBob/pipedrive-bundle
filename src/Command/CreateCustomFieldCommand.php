<?php

namespace Pipedrive\Command;

use Pipedrive\Entity\OrganizationField;
use Pipedrive\Interface\PipedriveEntityInterface;
use Pipedrive\Service\Pipedrive;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Yaml\Yaml;
use Zenstruck\Console\Attribute\Option;
use Zenstruck\Console\ConfigureWithAttributes;
use Zenstruck\Console\Invokable;
use Zenstruck\Console\IO;

use function Symfony\Component\String\u;

#[AsCommand(name: 'pipedrive:create-custom-field', description: 'Create a custom field into pipedrive')]
class CreateCustomFieldCommand extends Command
{
    use ConfigureWithAttributes;
    use Invokable;

    public function __construct(
        private readonly Pipedrive $pipedrive,
        private readonly SerializerInterface $serializer
    ) {
        parent::__construct();
    }

    public function __invoke(
        IO $io,
        #[Option(name: 'name')] string $name,
        #[Option(name: 'type')] string $type,
        #[Option(name: 'options')] array $options = []
    ): int {
        $field = new OrganizationField();
        $field->setName($name);
        $field->setFieldType($type);
        $field->setOptions($options);

        $customField = $this->pipedrive->createOrganizationField($field);

        if ($customField->isCustom()) {
            $io->writeln('Custom field created, please add this configuration to pipedrive.yaml config file');

            $customField = $this->pipedrive->getOnlineOrganizationCustomField($customField->getId());

            $id = u($customField->getName())->snake()->toString();
            $io->writeln('');
            $io->writeln('');
            $io->writeln(Yaml::dump([$id => $this->serializer->normalize($customField, null, ['groups' => PipedriveEntityInterface::SG_YAML])]));
            $io->writeln('');
        }

        return Command::SUCCESS;
    }
}
