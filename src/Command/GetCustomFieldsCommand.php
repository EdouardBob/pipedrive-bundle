<?php

namespace Pipedrive\Command;

use Pipedrive\Entity\OrganizationField;
use Pipedrive\Interface\PipedriveEntityInterface;
use Pipedrive\Service\Pipedrive;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Yaml\Yaml;
use Zenstruck\Console\Attribute\Option;
use Zenstruck\Console\ConfigureWithAttributes;
use Zenstruck\Console\Invokable;
use Zenstruck\Console\IO;

use function Symfony\Component\String\u;

#[AsCommand(name: 'pipedrive:get-custom-fields', description: 'Get custom fields from pipedrive')]
class GetCustomFieldsCommand extends Command
{
    use ConfigureWithAttributes;
    use Invokable;

    public function __construct(
        private readonly Pipedrive $pipedrive,
        private readonly SerializerInterface $serializer
    ) {
        parent::__construct();
    }

    public function __invoke(
        IO $io,
        #[Option(name: 'filter')] string $filter = null
    ): int {
        $customFields = [];

        /** @var OrganizationField $customField */
        foreach ($this->pipedrive->getOnlineOrganizationCustomFields() as $customField) {
            if (!$filter || $filter === $customField->getKey()) {
                $customFields[$customField->getKey()] = $customField;
            }
        }

        $table = new Table($io->output());
        $table->setHeaders(['Existing custom fields :']);

        foreach ($this->pipedrive->getAddedOrganizationCustomFields() as $key => $customField) {
            unset($customFields[$customField->getKey()]);
            $table->addRow([
                Yaml::dump([$key => $this->serializer->normalize($customField, null, ['groups' => PipedriveEntityInterface::SG_YAML])]),
            ]);
        }
        $table->render();

        if (!empty($customFields)) {
            $io->writeln('Custom fields that needs to be added :');

            foreach ($customFields as $customField) {
                $id = u($customField->getName())->snake()->toString();
                $io->writeln('');
                $io->writeln(Yaml::dump([$id => $this->serializer->normalize($customField, null, ['groups' => PipedriveEntityInterface::SG_YAML])]));
                $io->writeln('');
            }
        }

        /*
        $organization = $this->pipedrive->getOrganization(1);
        $organization->setCustomField($this->pipedrive->getOrganizationCustomField('super_id')?->setValue('684132468735'));
        $organization->setCustomField($this->pipedrive->getOrganizationCustomField(4023)?->setValue('3 Place Albert Camus, Nantes, France'));
        $this->pipedrive->updateOrganization($organization);
        */
        return Command::SUCCESS;
    }
}
