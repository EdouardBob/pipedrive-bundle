<?php

namespace Pipedrive\Component\Annotation;

use Attribute;
use DateTimeInterface;
use Pipedrive\Entity\CustomField;
use Pipedrive\Exceptions\CannotDefineValueException;
use Pipedrive\Interface\HasCustomFieldsInterface;
use Pipedrive\Interface\PipedriveEntityInterface;
use Throwable;
use ValueError;

#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_PROPERTY)]
class PipedriveSynchronizer
{
    private ?CustomField $customField = null;

    private HasCustomFieldsInterface $target;

    public function __construct(
        /** @See pipedrive.yaml */
        private readonly string|int $field,
    ) {
    }

    /**
     * @throws CannotDefineValueException
     */
    public function setValue(bool|int|string|DateTimeInterface|callable|null $value): CustomField
    {
        if ($this->customField instanceof CustomField && method_exists($this->customField, 'setValue')) {
            if (\is_callable($value)) {
                try {
                    $value = $value($this->target);
                } catch (Throwable) {
                    throw new CannotDefineValueException($this->customField);
                }
            }

            $this->customField->setValue($value);
            $this->target->setCustomField($this->customField);

            return $this->customField;
        }

        throw new ValueError(sprintf('Custom Field %s is not defined', $this->field));
    }

    /**
     * @param array<CustomField> $customFields
     */
    public function guessCustomField(array $customFields): ?CustomField
    {
        $this->customField = null;

        foreach ($customFields as $key => $customField) {
            if ($this->field === $key || $this->field === $customField->getKey() || $this->field === $customField->getId()) {
                $this->customField = $customField;
                break;
            }
        }

        return $this->customField;
    }

    public function getTarget(): HasCustomFieldsInterface
    {
        return $this->target;
    }

    public function setTarget(HasCustomFieldsInterface $target): void
    {
        $this->target = $target;
    }
}
