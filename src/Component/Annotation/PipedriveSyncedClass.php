<?php

namespace Pipedrive\Component\Annotation;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class PipedriveSyncedClass
{
    public function __construct(
        private readonly string $id_field,
        private readonly string $entityClass,
    ) {
    }

    public function getIdField(): string
    {
        return $this->id_field;
    }

    public function getEndpointBase(): string
    {
        return (new $this->entityClass())->getEndpointBase();
    }

    public function getEntityClass(): string
    {
        return $this->entityClass;
    }
}
