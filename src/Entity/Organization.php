<?php

namespace Pipedrive\Entity;

use DateTime;
use Pipedrive\Interface\HasCustomFieldsInterface;
use Pipedrive\Interface\PipedriveEntityInterface;
use Symfony\Component\Serializer\Annotation\Groups;

class Organization implements PipedriveEntityInterface, HasCustomFieldsInterface
{
    #[Groups([
        self::SG_UPDATE,
    ])]
    private ?int $id = null;

    private int $company_id = 108050;

    #[Groups([
        self::SG_UPDATE,
    ])]
    private string $name;
    private ?DateTime $update_time = null;
    private ?DateTime $delete_time = null;
    private ?DateTime $next_activity_date = null;
    private ?int $next_activity_id = null;
    private ?int $last_activity_id = null;
    private ?DateTime $last_activity_date = null;

    private ?string $address = null;
    private ?string $address_subpremise = null;
    private ?string $address_street_number = null;
    private ?string $address_route = null;
    private ?string $address_sublocality = null;
    private ?string $address_locality = null;
    private ?string $address_admin_area_level_1 = null;
    private ?string $address_admin_area_level_2 = null;
    private ?string $address_country = null;
    private ?string $address_postal_code = null;
    private ?string $address_formatted_address = null;

    private string $owner_name;
    private ?array $owner_id;

    #[Groups([
        self::SG_UPDATE,
    ])]
    private array $customFields = [];

    public function __construct()
    {
    }

    public function getEndpointBase(): string
    {
        return 'organizations';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCompanyId(): int
    {
        return $this->company_id;
    }

    public function setCompanyId(int $company_id): self
    {
        $this->company_id = $company_id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUpdateTime(): ?DateTime
    {
        return $this->update_time;
    }

    public function setUpdateTime(?DateTime $update_time): self
    {
        $this->update_time = $update_time;

        return $this;
    }

    public function getDeleteTime(): ?DateTime
    {
        return $this->delete_time;
    }

    public function setDeleteTime(?DateTime $delete_time): self
    {
        $this->delete_time = $delete_time;

        return $this;
    }

    public function getNextActivityDate(): ?DateTime
    {
        return $this->next_activity_date;
    }

    public function setNextActivityDate(?DateTime $next_activity_date): self
    {
        $this->next_activity_date = $next_activity_date;

        return $this;
    }

    public function getNextActivityId(): ?int
    {
        return $this->next_activity_id;
    }

    public function setNextActivityId(?int $next_activity_id): self
    {
        $this->next_activity_id = $next_activity_id;

        return $this;
    }

    public function getLastActivityId(): ?int
    {
        return $this->last_activity_id;
    }

    public function setLastActivityId(?int $last_activity_id): self
    {
        $this->last_activity_id = $last_activity_id;

        return $this;
    }

    public function getLastActivityDate(): ?DateTime
    {
        return $this->last_activity_date;
    }

    public function setLastActivityDate(?DateTime $last_activity_date): self
    {
        $this->last_activity_date = $last_activity_date;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddressSubpremise(): ?string
    {
        return $this->address_subpremise;
    }

    public function setAddressSubpremise(?string $address_subpremise): self
    {
        $this->address_subpremise = $address_subpremise;

        return $this;
    }

    public function getAddressStreetNumber(): ?string
    {
        return $this->address_street_number;
    }

    public function setAddressStreetNumber(?string $address_street_number): self
    {
        $this->address_street_number = $address_street_number;

        return $this;
    }

    public function getAddressRoute(): ?string
    {
        return $this->address_route;
    }

    public function setAddressRoute(?string $address_route): self
    {
        $this->address_route = $address_route;

        return $this;
    }

    public function getAddressSublocality(): ?string
    {
        return $this->address_sublocality;
    }

    public function setAddressSublocality(?string $address_sublocality): self
    {
        $this->address_sublocality = $address_sublocality;

        return $this;
    }

    public function getAddressLocality(): ?string
    {
        return $this->address_locality;
    }

    public function setAddressLocality(?string $address_locality): self
    {
        $this->address_locality = $address_locality;

        return $this;
    }

    public function getAddressAdminAreaLevel1(): ?string
    {
        return $this->address_admin_area_level_1;
    }

    public function setAddressAdminAreaLevel1(?string $address_admin_area_level_1): self
    {
        $this->address_admin_area_level_1 = $address_admin_area_level_1;

        return $this;
    }

    public function getAddressAdminAreaLevel2(): ?string
    {
        return $this->address_admin_area_level_2;
    }

    public function setAddressAdminAreaLevel2(?string $address_admin_area_level_2): self
    {
        $this->address_admin_area_level_2 = $address_admin_area_level_2;

        return $this;
    }

    public function getAddressCountry(): ?string
    {
        return $this->address_country;
    }

    public function setAddressCountry(?string $address_country): self
    {
        $this->address_country = $address_country;

        return $this;
    }

    public function getAddressPostalCode(): ?string
    {
        return $this->address_postal_code;
    }

    public function setAddressPostalCode(?string $address_postal_code): self
    {
        $this->address_postal_code = $address_postal_code;

        return $this;
    }

    public function getAddressFormattedAddress(): ?string
    {
        return $this->address_formatted_address;
    }

    public function setAddressFormattedAddress(?string $address_formatted_address): self
    {
        $this->address_formatted_address = $address_formatted_address;

        return $this;
    }

    public function getOwnerName(): string
    {
        return $this->owner_name;
    }

    public function setOwnerName(string $owner_name): self
    {
        $this->owner_name = $owner_name;

        return $this;
    }

    public function getOwnerId(): ?array
    {
        return $this->owner_id;
    }

    public function setOwnerId(?array $owner_id): self
    {
        $this->owner_id = $owner_id;

        return $this;
    }

    public function getCustomFields(): array
    {
        return $this->customFields;
    }

    public function __get($name): ?string
    {
        if (preg_match('/^[a-f0-9]{40}$/', (string) $name)) {
            return $this->customFields[$name] ?? null;
        }

        return null;
    }

    public function __set($name, $value)
    {
        if (preg_match('/^[a-f0-9]{40}$/', (string) $name)) {
            $this->customFields[$name] = $value;
        }
    }

    public function __isset($name)
    {
        return preg_match('/^[a-f0-9]{40}$/', (string) $name) && isset($this->customFields[$name]);
    }

    public function setCustomField(?CustomField $customField): self
    {
        if ($customField instanceof OrganizationField) {
            $this->customFields[$customField->getKey()] = $customField->getValue();
        }

        return $this;
    }

    public function setCustomFields(array $customFields): self
    {
        foreach ($customFields as $customField) {
            if ($customField instanceof OrganizationField) {
                $this->customFields[$customField->getKey()] = $customField->getValue();
            }
        }

        return $this;
    }
}
