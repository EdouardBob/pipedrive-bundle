<?php

namespace Pipedrive\Entity;

use Pipedrive\Interface\PipedriveEntityInterface;

class ResultList
{
    protected bool $success = false;

    /* @var PipedriveEntityInterface[] */
    private array $data = [];

    private bool $more_items_in_collection = false;
    private int $start = 0;
    private int $limit = 100;

    /**
     * @return PipedriveEntityInterface[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param PipedriveEntityInterface[] $data
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function hasMoreItemsInCollection(): bool
    {
        return $this->more_items_in_collection;
    }

    public function setMoreItemsInCollection(bool $more_items_in_collection): self
    {
        $this->more_items_in_collection = $more_items_in_collection;

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    public function getStart(): int
    {
        return $this->start;
    }

    public function setStart(int $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function count(): int
    {
        return \count($this->getData());
    }
}
