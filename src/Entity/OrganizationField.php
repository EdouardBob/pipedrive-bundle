<?php

namespace Pipedrive\Entity;

use DateTimeInterface;
use Pipedrive\Enum\CustomFieldType;
use ValueError;

class OrganizationField extends CustomField
{
    private bool|int|string|null $value = null;

    public function getEndpointBase(): string
    {
        return 'organizationFields';
    }

    public function getValue(): bool|int|string|null
    {
        return $this->value;
    }

    public function setValue(bool|int|string|DateTimeInterface|null $value): self
    {
        if (null !== $value) {
            if (CustomFieldType::ENUM === $this->field_type) {
                $valid = false;
                foreach ($this->getOptions() as $option) {
                    if ((string) $option['label'] === (string) $value || (string) $option['id'] === (string) $value) {
                        $valid = true;
                        $value = $option['id'];
                        break;
                    }
                }
                if (!$valid) {
                    throw new ValueError(sprintf('Value %s does not fit in enum', $value));
                }
            }

            if ($value instanceof DateTimeInterface) {
                if (CustomFieldType::TIME === $this->field_type) {
                    $value = $value->format('H:i:s');
                } else {
                    $value = $value->format('Y-m-d');
                }
            }
        }

        $this->value = $value;

        return $this;
    }
}
