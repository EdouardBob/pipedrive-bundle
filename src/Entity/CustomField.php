<?php

namespace Pipedrive\Entity;

use Pipedrive\Enum\CustomFieldType;
use Pipedrive\Interface\PipedriveEntityInterface;
use Stringable;
use Symfony\Component\Serializer\Annotation\Groups;

abstract class CustomField implements PipedriveEntityInterface, Stringable
{
    #[Groups([
        self::SG_UPDATE,
        self::SG_YAML,
    ])]
    private ?int $id = null;

    #[Groups([
        self::SG_CREATE,
        self::SG_UPDATE,
        self::SG_YAML,
    ])]
    private string $name;

    #[Groups([
        self::SG_CREATE,
        self::SG_UPDATE,
        self::SG_YAML,
    ])]
    private ?array $options = null;

    #[Groups([
        self::SG_CREATE,
        self::SG_UPDATE,
    ])]
    private bool $add_visible_flag;

    #[Groups([
        self::SG_CREATE,
        self::SG_UPDATE,
        self::SG_YAML,
    ])]
    protected ?CustomFieldType $field_type = null;

    #[Groups([
        self::SG_YAML,
    ])]
    private string $key;
    private int $order_nr;
    private bool $json_column_flag;

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function isAddVisibleFlag(): bool
    {
        return $this->add_visible_flag;
    }

    public function setAddVisibleFlag(bool $add_visible_flag): self
    {
        $this->add_visible_flag = $add_visible_flag;

        return $this;
    }

    public function getFieldType(): ?string
    {
        return $this->field_type?->value;
    }

    public function setFieldType(string|CustomFieldType|null $field_type): self
    {
        $this->field_type = $field_type instanceof CustomFieldType ? $field_type : CustomFieldType::from($field_type);

        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getOrderNr(): int
    {
        return $this->order_nr;
    }

    public function setOrderNr(int $order_nr): self
    {
        $this->order_nr = $order_nr;

        return $this;
    }

    public function isJsonColumnFlag(): bool
    {
        return $this->json_column_flag;
    }

    public function setJsonColumnFlag(bool $json_column_flag): self
    {
        $this->json_column_flag = $json_column_flag;

        return $this;
    }

    public function isCustom(): bool
    {
        return null !== $this->getId() && preg_match('/^[a-f0-9]{40}$/', $this->getKey());
    }
}
