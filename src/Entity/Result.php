<?php

namespace Pipedrive\Entity;

use Pipedrive\Interface\HasCustomFieldsInterface;
use Pipedrive\Interface\PipedriveEntityInterface;

class Result
{
    protected bool $success = false;
    private PipedriveEntityInterface $data;

    public function getData(): PipedriveEntityInterface|HasCustomFieldsInterface
    {
        return $this->data;
    }

    public function setData(PipedriveEntityInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}
