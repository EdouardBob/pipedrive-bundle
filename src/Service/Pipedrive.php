<?php

namespace Pipedrive\Service;

use DateTimeInterface;
use Pipedrive\Component\Annotation\PipedriveSyncedClass;
use Pipedrive\Component\Annotation\PipedriveSynchronizer;
use Pipedrive\Entity\Organization;
use Pipedrive\Entity\OrganizationField;
use Pipedrive\Exceptions\CannotDefineValueException;
use Pipedrive\Interface\HasCustomFieldsInterface;
use Pipedrive\Interface\PipedriveEntityInterface;
use Pipedrive\Interface\PipedriveSyncedInterface;
use Pipedrive\Service\API\OrganizationFieldPipedriveApiClient;
use Pipedrive\Service\API\OrganizationPipedriveApiClient;
use Pipedrive\Service\API\PipedriveApiClient;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

use function Symfony\Component\String\u;

class Pipedrive implements ServiceSubscriberInterface
{
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly ParameterBagInterface $parameterBag,
        private readonly DenormalizerInterface $denormalizer,
    ) {
    }

    public static function getSubscribedServices(): array
    {
        return [
            PipedriveApiClient::class,
            OrganizationPipedriveApiClient::class,
            OrganizationFieldPipedriveApiClient::class,
        ];
    }

    /**
     * @throws ExceptionInterface
     * @throws CannotDefineValueException
     */
    public function createCustomFieldSynchronizer(string|int|PipedriveSynchronizer $mixed, HasCustomFieldsInterface $target = null, bool|int|string|DateTimeInterface|callable $value = null): PipedriveSynchronizer
    {
        if (\is_scalar($mixed)) {
            $mixed = new PipedriveSynchronizer($mixed);
        }
        $mixed->guessCustomField($this->getAddedOrganizationCustomFields());

        if ($target instanceof HasCustomFieldsInterface) {
            $mixed->setTarget($target);
            $mixed->setValue($value);
        }

        return $mixed;
    }

    private function getOrganizationApi(): OrganizationPipedriveApiClient
    {
        return $this->container->get(OrganizationPipedriveApiClient::class);
    }

    private function getOrganizationFieldApi(): OrganizationFieldPipedriveApiClient
    {
        return $this->container->get(OrganizationFieldPipedriveApiClient::class);
    }

    /**
     * @return array<OrganizationField>
     *
     * @throws ExceptionInterface
     */
    public function getAddedOrganizationCustomFields(): array
    {
        return $this->denormalizer->denormalize($this->parameterBag->get('pipedrive.custom_fields.organization'), OrganizationField::class.'[]');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getOnlineOrganizationCustomFields(): array
    {
        return $this->getOrganizationFieldApi()->getAll()->getData();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getOnlineOrganizationCustomField(int $id): OrganizationField
    {
        return $this->getOrganizationFieldApi()->getDetail($id);
    }

    public function getOrganizationCustomField(string|int $q): ?OrganizationField
    {
        foreach ($this->getAddedOrganizationCustomFields() as $key => $customField) {
            if ($q === $key || $q === $customField->getKey() || $customField->getId() === $q) {
                return $customField;
            }
        }

        return null;
    }

    public function getOrganization(int $id): Organization
    {
        return $this->getOrganizationApi()->getDetail($id);
    }

    public function updateOrganization(Organization $organization): Organization
    {
        return $this->getOrganizationApi()->update($organization);
    }

    public function createOrganizationField(OrganizationField $field): OrganizationField
    {
        return $this->getOrganizationFieldApi()->create($field);
    }

    public function apply(PipedriveSyncedInterface|PipedriveEntityInterface $object): ?HasCustomFieldsInterface
    {
        return $this->sync($object, false);
    }

    public function sync(PipedriveSyncedInterface|PipedriveEntityInterface $object, bool $apply = true): ?HasCustomFieldsInterface
    {
        if ($object instanceof HasCustomFieldsInterface) {
            $client = $this->guessClient($object);

            return $apply ? $client?->update($object) : $object;
        }

        if ($object instanceof PipedriveSyncedInterface) {
            $target = $this->getTarget($object);
            if ($target instanceof HasCustomFieldsInterface) {
                $reflectedController = new ReflectionClass($object);
                foreach ($reflectedController->getMethods() as $method) {
                    foreach ($method->getAttributes() as $attribute) {
                        if (PipedriveSynchronizer::class === $attribute->getName()) {
                            $synchronizer = $this->createCustomFieldSynchronizer($attribute->newInstance());
                            $synchronizer->setTarget($target);
                            $synchronizer->setValue($method->getClosure($object)->call($object));
                        }
                    }
                }
                foreach ($reflectedController->getProperties() as $property) {
                    foreach ($property->getAttributes() as $attribute) {
                        if (PipedriveSynchronizer::class === $attribute->getName()) {
                            $synchronizer = $this->createCustomFieldSynchronizer($attribute->newInstance());
                            $synchronizer->setTarget($target);
                            $synchronizer->setValue($property->getValue($object));
                        }
                    }
                }

                $client = $this->guessClient($object);

                return $apply ? $client?->update($target) : $target;
            }
        }

        return null;
    }

    public function getTarget(PipedriveSyncedInterface $object): ?HasCustomFieldsInterface
    {
        $id = $this->getSyncId($object);
        if (null !== $id) {
            $client = $this->guessClient($object);
            if ($client instanceof PipedriveApiClient) {
                return $client->getDetail($id);
            }
        }

        return null;
    }

    private function getSyncId(PipedriveSyncedInterface $object): int|string|null
    {
        $reflectedController = new ReflectionClass($object);

        foreach ($reflectedController->getAttributes() as $attribute) {
            if (PipedriveSyncedClass::class === $attribute->getName()) {
                /** @var PipedriveSyncedClass $synchronizer */
                $synchronizer = $attribute->newInstance();
                $getter = u('get_'.$synchronizer->getIdField())->camel()->toString();
                foreach ($reflectedController->getMethods() as $method) {
                    if ($method->getName() === $getter) {
                        return $method->getClosure($object)->call($object);
                    }
                }
                foreach ($reflectedController->getProperties() as $property) {
                    if ($property->getName() === $synchronizer->getIdField()) {
                        return $property->getValue($object);
                    }
                }
            }
        }

        return null;
    }

    private function guessClient(PipedriveSyncedInterface|PipedriveEntityInterface $object): ?PipedriveApiClient
    {
        /** @var PipedriveApiClient $client */
        $client = $this->container->get(PipedriveApiClient::class);

        if ($object instanceof PipedriveSyncedInterface) {
            $reflectedController = new ReflectionClass($object);

            foreach ($reflectedController->getAttributes() as $attribute) {
                if (PipedriveSyncedClass::class === $attribute->getName()) {
                    /** @var PipedriveSyncedClass $synchronizer */
                    $synchronizer = $attribute->newInstance();

                    $client->setEndpointBase($synchronizer->getEndpointBase());
                    $client->setEntityClass($synchronizer->getEntityClass());

                    return $client;
                }
            }

            return null;
        }

        $client->setEndpointBase($object->getEndpointBase());
        $client->setEntityClass($object::class);

        return $client;
    }
}
