<?php

namespace Pipedrive\Service\API;

use Pipedrive\Entity\ResultList;
use Pipedrive\Interface\HasCustomFieldsInterface;

/**
 * @method ResultList<HasCustomFieldsInterface> getAll()
 * @method HasCustomFieldsInterface             getDetail(string $id)
 * @method HasCustomFieldsInterface             create(HasCustomFieldsInterface $entity)
 * @method HasCustomFieldsInterface             update(HasCustomFieldsInterface $entity)
 * @method void                                 remove(string $id)
 */
final class PipedriveApiClient extends AbstractPipedriveApiClient
{
    public function setEndpointBase(string $endpointBase): self
    {
        $this->endpointBase = $endpointBase;

        return $this;
    }

    public function setEntityClass(string $entityClass): void
    {
        $this->entityClass = $entityClass;
    }
}
