<?php

namespace Pipedrive\Service\API;

use Pipedrive\Entity\Result;
use Pipedrive\Entity\ResultList;
use Pipedrive\Interface\HasCustomFieldsInterface;
use Pipedrive\Interface\PipedriveEntityInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractPipedriveApiClient
{
    use PipedriveApiClientTrait;

    private readonly string $url;

    protected string $endpointBase = '';

    protected string $entityClass = '';

    protected string $sgCreate = PipedriveEntityInterface::SG_CREATE;

    protected string $sgUpdate = PipedriveEntityInterface::SG_UPDATE;

    public function __construct(
        HttpClientInterface $httpClient,
        ParameterBagInterface $parameterBag,
        protected readonly SerializerInterface $serializer,
        protected readonly LoggerInterface $apiCallLogger
    ) {
        $this->setHttpClient($httpClient);
        $this->setPipedriveApiToken($parameterBag->get('pipedrive.config.api_token'));

        $this->url = $parameterBag->get('pipedrive.config.url');
    }

    public function supports(string|object $subject): bool
    {
        if (\is_object($subject)) {
            return $subject::class === $this->entityClass;
        }

        return $subject === $this->entityClass;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getAll(): ResultList
    {
        $start = 0;
        $limit = 100;

        $data = [];

        do {
            $list = $this->getList($start, $limit);

            foreach ($list->getData() as $item) {
                $data[] = $item;
            }
            $start += $limit;
        } while ($list->hasMoreItemsInCollection());

        $result = new ResultList();
        $result->setSuccess(true);
        $result->setData($data);

        return $result;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     *
     * @internal
     */
    public function getList(int $start = 0, int $limit = 100): ResultList
    {
        $url = $this->getBaseUrl().'?start='.$start.'&limit='.$limit;
        $response = $this->call(Request::METHOD_GET, $url);

        // @See PipedriveResultDenormalizer
        return $this->serializer->deserialize($response->getContent(), ResultList::class.'<'.$this->entityClass.'>', 'json');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getDetail(string $id): PipedriveEntityInterface|null
    {
        $url = $this->getBaseUrl().$id.'/';
        $response = $this->call(Request::METHOD_GET, $url);

        /** @var Result $result */
        $result = $this->serializer->deserialize($response->getContent(), Result::class.'<'.$this->entityClass.'>', 'json');

        return $result->isSuccess() ? $result->getData() : null;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function create(PipedriveEntityInterface $entity): HasCustomFieldsInterface|PipedriveEntityInterface|null
    {
        $url = $this->getBaseUrl();

        $requestBody = $this->serializer->serialize($entity, 'json', [
            'groups' => [$this->sgCreate],
        ]);

        $response = $this->call(Request::METHOD_POST, $url, [
            'body' => $requestBody,
        ]);
        $responseContent = $response->getContent(false);

        /** @var Result $result */
        $result = $this->serializer->deserialize($responseContent, Result::class.'<'.$this->entityClass.'>', 'json', ['groups' => PipedriveEntityInterface::SG_YAML]);

        return $result->isSuccess() ? $result->getData() : null;
    }

    /**
     * @throws ClientExceptionInterface|RedirectionExceptionInterface|TransportExceptionInterface|ServerExceptionInterface
     */
    public function update(HasCustomFieldsInterface $entity): HasCustomFieldsInterface|PipedriveEntityInterface|null
    {
        $url = $this->getBaseUrl().$entity->getId().'/';
        $requestBody = $this->serializer->serialize($entity, 'json', [
            'groups' => [$this->sgUpdate],
        ]);
        $response = $this->call(Request::METHOD_PUT, $url, [
            'body' => $requestBody,
        ]);

        $responseContent = $response->getContent(false);

        /** @var Result $result */
        $result = $this->serializer->deserialize($responseContent, Result::class.'<'.$this->entityClass.'>', 'json');

        return $result->isSuccess() ? $result->getData() : null;
    }

    public function remove(string $id): void
    {
        $url = $this->getBaseUrl().$id.'/';
        $this->call(Request::METHOD_DELETE, $url);
    }

    protected function getBaseUrl(): string
    {
        return trim($this->url, '/').'/'.$this->endpointBase.'/';
    }
}
