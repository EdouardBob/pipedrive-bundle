<?php

namespace Pipedrive\Service\API;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

trait PipedriveApiClientTrait
{
    private string $pipedriveApiToken;

    private HttpClientInterface $httpClient;

    protected function setPipedriveApiToken(string $token)
    {
        $this->pipedriveApiToken = $token;
    }

    protected function setHttpClient(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    protected function call(string $method, string $url, array $options = []): ResponseInterface
    {
        if (!isset($options['headers'])) {
            $options['headers'] = [];
        }

        $options['headers']['Accept'] = 'application/json';
        $options['headers']['Content-Type'] = 'application/json';

        // No use of api token in the query string
        $options['headers']['X-API-TOKEN'] = $this->pipedriveApiToken;

        $response = $this->httpClient->request($method, $url, $options);

        $this->apiCallLogger->info('Pipedrive API Call', [
            'request' => [
                'method' => $method,
                'url' => $url,
                'headers' => $options['headers'],
                'body' => $options['body'] ?? null,
            ],
            'response' => [
                'statusCode' => $response->getStatusCode(),
                'headers' => $response->getHeaders(false),
                'body' => $response->getContent(false),
            ],
        ]);

        return $response;
    }
}
