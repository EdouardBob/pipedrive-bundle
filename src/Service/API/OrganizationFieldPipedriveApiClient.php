<?php

namespace Pipedrive\Service\API;

use Pipedrive\Entity\OrganizationField;
use Pipedrive\Entity\ResultList;

/**
 * @method ResultList<OrganizationField> getAll()
 * @method OrganizationField             getDetail(string $id)
 * @method OrganizationField             create(OrganizationField $entity)
 * @method OrganizationField             update(OrganizationField $entity)
 * @method void                          remove(string $id)
 */
class OrganizationFieldPipedriveApiClient extends AbstractPipedriveApiClient
{
    protected string $endpointBase = 'organizationFields';

    protected string $entityClass = OrganizationField::class;
}
