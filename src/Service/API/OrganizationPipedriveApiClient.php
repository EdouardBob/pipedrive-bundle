<?php

namespace Pipedrive\Service\API;

use Pipedrive\Entity\Organization;
use Pipedrive\Entity\ResultList;

/**
 * @method ResultList<Organization> getAll()
 * @method Organization             getDetail(string $id)
 * @method Organization             create(Organization $entity)
 * @method Organization             update(Organization $entity)
 * @method void                     remove(string $id)
 */
class OrganizationPipedriveApiClient extends AbstractPipedriveApiClient
{
    protected string $endpointBase = 'organizations';

    protected string $entityClass = Organization::class;
}
