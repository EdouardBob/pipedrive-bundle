<?php

namespace Pipedrive\Normalizer;

use ArrayObject;
use Pipedrive\Entity\Result;
use Pipedrive\Entity\ResultList;
use Pipedrive\Interface\PipedriveEntityInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PipedriveResultDenormalizer implements NormalizerInterface, DenormalizerInterface, DenormalizerAwareInterface, NormalizerAwareInterface, CacheableSupportsMethodInterface
{
    use DenormalizerAwareTrait;
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'PIPEDRIVE_OBJECT_NORMALIZER_ALREADY_CALLED';

    public function getSupportedTypes(?string $format): array
    {
        return [
            '*' => false,
        ];
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        return str_starts_with($type, ResultList::class) || str_starts_with($type, Result::class);
    }

    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return !isset($context[self::ALREADY_CALLED]) && $data instanceof PipedriveEntityInterface;
    }

    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): Result|ResultList|null
    {
        if (str_starts_with($type, ResultList::class)) {
            $entityType = str_replace([ResultList::class.'<', '>'], '', $type);

            $resultList = new ResultList();

            if (isset($data['success'])) {
                $resultList->setSuccess((bool) $data['success']);
            }

            if (isset($data['data'])) {
                $resultList->setData($this->denormalizer->denormalize($data['data'], $entityType.'[]', $format, $context));
            }

            if (isset($data['additional_data']['pagination']['more_items_in_collection'])) {
                $resultList->setMoreItemsInCollection($data['additional_data']['pagination']['more_items_in_collection']);
            }
            if (isset($data['additional_data']['pagination']['start'])) {
                $resultList->setStart((int) $data['additional_data']['pagination']['start']);
            }
            if (isset($data['additional_data']['pagination']['limit'])) {
                $resultList->setLimit((int) $data['additional_data']['pagination']['limit']);
            }

            return $resultList;
        }
        if (str_starts_with($type, Result::class)) {
            $entityType = str_replace([Result::class.'<', '>'], '', $type);

            $result = new Result();

            $result->setSuccess($data['success'] ?? false);
            if (isset($data['data'])) {
                $result->setData($this->denormalizer->denormalize($data['data'], $entityType, $format, $context));
            }

            return $result;
        }

        return null;
    }

    public function normalize(mixed $object, string $format = null, array $context = []): array|bool|string|int|float|null|ArrayObject
    {
        $context[self::ALREADY_CALLED] = true;

        $data = $this->normalizer->normalize($object, $format, $context);
        if (\is_array($data)) {
            if (isset($data['custom_fields']) && is_iterable($data['custom_fields'])) {
                foreach ($data['custom_fields'] as $customFieldId => $customFieldValue) {
                    $data[$customFieldId] = $customFieldValue;
                }
                unset($data['custom_fields']);
            }

            if (isset($data['customFields']) && is_iterable($data['customFields'])) {
                foreach ($data['customFields'] as $customFieldId => $customFieldValue) {
                    $data[$customFieldId] = $customFieldValue;
                }
                unset($data['customFields']);
            }
        }

        return $data;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return false;
    }
}
