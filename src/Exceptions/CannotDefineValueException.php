<?php

namespace Pipedrive\Exceptions;

use Exception;
use Pipedrive\Entity\CustomField;
use Throwable;

class CannotDefineValueException extends Exception
{
    public function __construct(private readonly CustomField $customField, string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getCustomField(): CustomField
    {
        return $this->customField;
    }
}
