<?php

namespace Pipedrive\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('pipedrive');

        // @formatter:off
        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('url')->end()
                ->scalarNode('api_token')->end()
                ->arrayNode('custom_fields')
                    ->children()
                        ->arrayNode('organization')
                            ->arrayPrototype()
                                ->children()
                                    ->scalarNode('id')->isRequired()->cannotBeEmpty()->end()
                                    ->scalarNode('name')->isRequired()->cannotBeEmpty()->end()
                                    ->scalarNode('key')->isRequired()->cannotBeEmpty()->end()
                                    ->scalarNode('field_type')->end()
                                    ->arrayNode('options')
                                            ->arrayPrototype()
                                            ->children()
                                            ->scalarNode('id')->isRequired()->cannotBeEmpty()->end()
                                            ->scalarNode('label')->isRequired()->cannotBeEmpty()->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        // @formatter:on

        return $treeBuilder;
    }
}
