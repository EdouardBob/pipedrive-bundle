<?php

namespace Pipedrive\Interface;

use Pipedrive\Entity\CustomField;

interface HasCustomFieldsInterface
{
    public function getId(): ?int;

    public function setCustomField(?CustomField $customField): PipedriveEntityInterface;

    public function setCustomFields(array $customFields): PipedriveEntityInterface;
}
