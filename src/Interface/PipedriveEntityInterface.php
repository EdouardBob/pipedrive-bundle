<?php

namespace Pipedrive\Interface;

interface PipedriveEntityInterface
{
    public const SG_CREATE = 'SG_PIPEDRIVE_GROUP_CREATE';
    public const SG_UPDATE = 'SG_PIPEDRIVE_GROUP_UPDATE';
    public const SG_YAML = 'SG_PIPEDRIVE_GROUP_YAML';

    public function getId(): ?int;

    public function getEndpointBase(): string;
}
