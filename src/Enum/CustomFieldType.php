<?php

namespace Pipedrive\Enum;

enum CustomFieldType: string
{
    case ADDRESS = 'address';
    case DATE = 'date';
    case DATERANGE = 'daterange';
    case DOUBLE = 'double';
    case ENUM = 'enum';
    case INT = 'int';
    case MONETARY = 'monetary';
    case ORG = 'org';
    case PEOPLE = 'people';
    case PICTURE = 'picture';
    case PHONE = 'phone';
    case SET = 'set';
    case TEXT = 'text';
    case TIME = 'time';
    case TIMERANGE = 'timerange';
    case USER = 'user';
    case VARCHAR = 'varchar';
    case VARCHAR_AUTO = 'varchar_auto';
    case VISIBLE_TO = 'visible_to';
}
