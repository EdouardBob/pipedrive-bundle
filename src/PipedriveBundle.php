<?php

namespace Pipedrive;

use Pipedrive\DependencyInjection\PipedriveExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PipedriveBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new PipedriveExtension();
    }
}
