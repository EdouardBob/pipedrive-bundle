<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('var');

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
        'no_unused_imports' => false,
        'global_namespace_import' => true,
        'native_function_invocation' => true,
        'ternary_to_elvis_operator' => true,
        'modernize_types_casting' => true,
        'non_printable_character' => true,
        'is_null' => true,
        'no_useless_sprintf' => true,
        'self_accessor' => true,
        'no_trailing_whitespace_in_string' => true,
        'string_line_ending' => true,
        'set_type_to_cast' => true,
        'no_alias_functions' => true,
        'native_constant_invocation' => true,
        'logical_operators' => true,
        'ordered_traits' => false,
        'string_length_to_empty' => true,
        'function_to_constant' => true,
        'array_push' => true,
    ])
    ->setFinder($finder)
    ->setLineEnding("\n");
